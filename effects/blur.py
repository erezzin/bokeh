import cv2
import numpy as np

from color_tools import ColorTools
from effects.bokeh import BokehEffect
from video_effect import VideoEffect


class BlurOthersEffect(VideoEffect):

    def __init__(self, hue_neighborhood, max_size=0):
        super().__init__(True)
        self.max_size = max_size
        self.hue_neighborhood = hue_neighborhood
        self.x = None
        self.y = None

    @staticmethod
    def blur_by_mask(frame, mask):
        not_mask = ~mask
        blurred = cv2.GaussianBlur(frame, (0, 0), 3)
        frame[not_mask] = blurred[not_mask]

    def apply(self, frame):
        if self.x is None:
            return
        hsv = ColorTools.bgr_to_hsv(frame)
        mask = ColorTools.get_mask_component_at_pos_by_hue(hsv, self.x, self.y, self.hue_neighborhood)

        if self.max_size > 0:
            mask &= BokehEffect.get_radial_mask((self.x, self.y), self.max_size, frame.shape)

        BlurOthersEffect.blur_by_mask(frame, mask)

    def on_mouse_click(self, frame, x, y, is_left):
        self.x, self.y = x, y

    @staticmethod
    def get_radial_mask(center_p, radius, shape):
        X, Y = np.arange(0, shape[1]), np.arange(0, shape[0])
        X, Y = np.meshgrid(X, Y)
        return (X - center_p[0]) ** 2 + (Y - center_p[1]) ** 2 <= (radius ** 2)


class BlurBackground(BlurOthersEffect):

    def __init__(self, hue_median, threshold=15, hue_neighborhood=20, minimal_radius=10):
        super().__init__(hue_neighborhood)
        self.minimal_radius = minimal_radius
        self.threshold = threshold
        self.hue_median = hue_median

    def apply(self, frame):
        hsv = ColorTools.bgr_to_hsv(frame)

        components = ColorTools.convert_boolean_map_to_uint8_map(
            np.abs(hsv[:, :, 0].astype(int) - self.hue_median) <= self.threshold
        )
        # Remove the too-fine components
        components &= cv2.morphologyEx(components, cv2.MORPH_CLOSE, np.ones((2 * self.minimal_radius,) * 2))

        n_labels, labels, stats, centroids = cv2.connectedComponentsWithStats(components, 8, cv2.CV_32S)

        BokehEffect.blur_by_mask(frame, labels == 0)