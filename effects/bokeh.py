import cv2
import numpy as np

from color_tools import ColorTools
from effects.blur import BlurOthersEffect

class BokehEffect(BlurOthersEffect):

    def __init__(self, hue_median, threshold=10, max_size=0, hue_neighborhood=20, minimal_radius=10):
        super().__init__(hue_neighborhood)
        self.max_size = max_size
        self.minimal_radius = minimal_radius
        self.threshold = threshold
        self.hue_median = hue_median

        self.x, self.y = None, None
        self._is_background = None

        self._behavior_initialized = False

    def apply(self, frame):
        if self.x is None:
            return

        hsv = ColorTools.bgr_to_hsv(frame)

        components = ColorTools.convert_boolean_map_to_uint8_map(
            np.abs(hsv[:, :, 0].astype(int) - self.hue_median) >= self.threshold
        )

        if self.max_size > 0:
            components &= BokehEffect.get_radial_mask((self.x, self.y), self.max_size, frame.shape)

        # Remove the too-fine components
        components &= cv2.morphologyEx(components, cv2.MORPH_OPEN, np.ones((2 * self.minimal_radius,) * 2))
        # Make the shape more complete
        components |= cv2.morphologyEx(components, cv2.MORPH_CLOSE, np.ones((2 * self.minimal_radius,) * 2))

        # if not self._behavior_initialized:
        #     self._is_background = components[self.y, self.x] == 0  # 0 is AKA Background
        #     self._behavior_initialized = True

        # if self._is_background:
        if components[self.y, self.x] == 0:  # 0 is AKA Background

            mask = ColorTools.get_mask_component_at_pos_by_hue(self.hue_median, self.x, self.y, self.threshold, True)
            mask &= (components == 0)

        else:
            n_labels, labels, stats, centroids = cv2.connectedComponentsWithStats(components, 8, cv2.CV_32S)

            label = labels[self.y, self.x]

            # if label == 0: # This is "back being" background
            #     self._is_background = 0
            #     return self.apply(self, frame)

            self.x, self.y = centroids[label].astype(int)
            mask = labels == label

            if self.max_size > 0:
                mask &= BokehEffect.get_radial_mask((self.x, self.y), self.max_size, frame.shape)

        BokehEffect.blur_by_mask(frame, mask)


class BokehEffectNoMedian(BlurOthersEffect):

    def __init__(self, threshold=10, max_size=0, hue_neighborhood=20, minimal_radius=10):
        super().__init__(hue_neighborhood)
        self.minimal_radius = minimal_radius
        self.threshold = threshold

        self.x, self.y = None, None
        self.prev_frame = None

        self.hue_at_point = None

    def apply(self, frame):
        if self.x is None:
            return

        hsv = ColorTools.bgr_to_hsv(frame)

        self.hue_at_point = hsv[self.y, self.x, 0]

        components = ColorTools.convert_boolean_map_to_uint8_map(
            ColorTools.is_in_neighborhood_of_hue(hsv[:, :, 0], self.hue_at_point, self.threshold)
        )

        if self.max_size > 0:
            components &= \
                BokehEffectNoMedian.get_radial_mask(np.array([self.x, self.y]), self.max_size, components.shape)

        # Remove the too-fine components
        components &= cv2.morphologyEx(components, cv2.MORPH_OPEN, np.ones((2 * self.minimal_radius,) * 2))

        n_labels, labels, stats, centroids = cv2.connectedComponentsWithStats(components, 8, cv2.CV_32S)

        label = labels[self.y, self.x]

        self.x, self.y = centroids[label].astype(int)
        mask = labels == label

        BokehEffectNoMedian.blur_by_mask(frame, mask)

    def on_mouse_click(self, frame, x, y, is_left):
        self.x, self.y = x, y