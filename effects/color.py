from color_tools import ColorTools
from video_effect import VideoEffect


class HueOnlyEffect(VideoEffect):
    def apply(self, frame):
        hsv = ColorTools.bgr_to_hsv(frame)
        hsv[:, :, 2] = 0xFF
        hsv[:, :, 1] = 0xFF
        frame[:] = ColorTools.hsv_to_bgr(hsv)[:]


class DesaturateOthersEffect(VideoEffect):

    def __init__(self, hue_neighborhood):
        super().__init__(True)
        self.hue_neighborhood = hue_neighborhood
        self.x = None
        self.y = None

    def apply(self, frame):
        if self.x is None:
            return
        hsv = ColorTools.bgr_to_hsv(frame)

        mask = ColorTools.get_mask_component_at_pos_by_hue(frame, self.x, self.y, self.hue_neighborhood)

        hsv[:, :, 1] &= mask

        frame[:] = ColorTools.hsv_to_bgr(hsv)[:]

    def on_mouse_click(self, frame, x, y, is_left):
        self.x, self.y = x, y