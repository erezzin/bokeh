import cv2
import numpy as np
from scipy.ndimage import measurements as snm


class ColorTools:
    @staticmethod
    def bgr_to_hsv(frame):
        return cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    @staticmethod
    def hsv_to_bgr(frame):
        return cv2.cvtColor(frame, cv2.COLOR_HSV2BGR)

    @staticmethod
    def get_all_component_with_hue(hue_mask, hue_val, hue_neighborhood):

        mask = cv2.inRange(hue_mask,
                           np.array([hue_val - hue_neighborhood]),
                           np.array([hue_val + hue_neighborhood]))
        # HUE is cyclic
        if hue_val > 255 - hue_neighborhood:
            mask = mask | ((hue_mask <= hue_neighborhood + (hue_val - 0xFF)).astype("uint8") * 0xFF)
        elif hue_val < hue_neighborhood:
            mask = mask | ((hue_mask >= (0xFF - (hue_neighborhood - hue_val))).astype("uint8") * 0xFF)

        return mask

    @staticmethod
    def is_in_neighborhood_of_hue(hue_to_check: np.ndarray, hue_val: np.ndarray, hue_neighborhood: np.int) -> np.ndarray:
        hue_to_check = hue_to_check - hue_val
        # hue is cyclic
        return (hue_to_check <= hue_neighborhood) | (hue_to_check >= 255 - hue_neighborhood)

    @staticmethod
    def get_mask_component_at_pos_by_hue(hsv, x, y, hue_neighborhood, input_is_only_hue=False):

        # Only hue can MAKE .all() this world seem .right()
        # Only hue can MAKE the darkness (== low value) bright (== high value)
        # Only hue and hue alone can thrill 'self' like hue do
        # And fill self.heart with love for (... in ... :) only hue
        if input_is_only_hue:
            pass  # I like this form better because otherwise the song will not fit.
        else:
            hsv = hsv[:, :, 0]

        mask = ColorTools.get_all_component_with_hue(hsv, hsv[y, x], hue_neighborhood)

        connected, _ = snm.label(mask, np.ones((3, 3)))
        return connected[y, x] == connected

    @staticmethod
    def convert_boolean_map_to_uint8_map(bmap):
        return bmap.astype("uint8") * 0xFF