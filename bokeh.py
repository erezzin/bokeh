import time

import cv2
import cv2.cv2
import numpy as np
import scipy.ndimage.measurements as snm


class VideoEffect:
    def __init__(self, mouse_click_integration=False):
        self.mouse_click_integration = mouse_click_integration

    def apply(self, frame):
        pass

    def on_mouse_click(self, frame, x, y, is_left):
        pass


class BlurOthersEffect(VideoEffect):

    def __init__(self, hue_neighborhood):
        super().__init__(True)
        self.hue_neighborhood = hue_neighborhood
        self.x = None
        self.y = None

    @staticmethod
    def blur_by_mask(frame, mask):
        not_mask = ~mask
        blurred = cv2.GaussianBlur(frame, (0, 0), 3)
        frame[not_mask] = blurred[not_mask]

    def apply(self, frame):
        if self.x is None:
            return
        hsv = ColorTools.bgr_to_hsv(frame)
        mask = ColorTools.get_mask_component_at_pos_by_hue(hsv, self.x, self.y, self.hue_neighborhood)
        BlurOthersEffect.blur_by_mask(frame, mask)

    def on_mouse_click(self, frame, x, y, is_left):
        self.x, self.y = x, y


class BlurBackground(BlurOthersEffect):

    def __init__(self, hue_median, threshold=15, hue_neighborhood=20, minimal_radius=10):
        super().__init__(hue_neighborhood)
        self.minimal_radius = minimal_radius
        self.threshold = threshold
        self.hue_median = hue_median

    def apply(self, frame):
        hsv = ColorTools.bgr_to_hsv(frame)

        components = ColorTools.convert_boolean_map_to_uint8_map(
            np.abs(hsv[:, :, 0].astype(int) - self.hue_median) <= self.threshold
        )
        # Remove the too-fine components
        components &= cv2.morphologyEx(components, cv2.MORPH_CLOSE, np.ones((2 * self.minimal_radius,) * 2))

        n_labels, labels, stats, centroids = cv2.connectedComponentsWithStats(components, 8, cv2.CV_32S)

        BokehEffect.blur_by_mask(frame, labels == 0)


class BokehEffect(BlurOthersEffect):

    def __init__(self, hue_median, threshold=10, max_size=0, hue_neighborhood=20, minimal_radius=10):
        super().__init__(hue_neighborhood)
        self.max_size = max_size
        self.minimal_radius = minimal_radius
        self.threshold = threshold
        self.hue_median = hue_median

        self.x, self.y = None, None
        self._is_background = None

        self._behavior_initialized = False

    def apply(self, frame):
        if self.x is None:
            return

        hsv = ColorTools.bgr_to_hsv(frame)

        components = ColorTools.convert_boolean_map_to_uint8_map(
            np.abs(hsv[:, :, 0].astype(int) - self.hue_median) >= self.threshold
        )
        # Remove the too-fine components
        components &= cv2.morphologyEx(components, cv2.MORPH_OPEN, np.ones((2 * self.minimal_radius,) * 2))
        # Make the shape more complete
        components |= cv2.morphologyEx(components, cv2.MORPH_CLOSE, np.ones((2 * self.minimal_radius,) * 2))

        # if not self._behavior_initialized:
        #     self._is_background = components[self.y, self.x] == 0  # 0 is AKA Background
        #     self._behavior_initialized = True

        # if self._is_background:
        if components[self.y, self.x] == 0: # 0 is AKA Background

            mask = ColorTools.get_mask_component_at_pos_by_hue(self.hue_median, self.x, self.y, self.threshold, True)
            mask &= (components == 0)

        else:
            n_labels, labels, stats, centroids = cv2.connectedComponentsWithStats(components, 8, cv2.CV_32S)

            label = labels[self.y, self.x]

            # if label == 0: # This is "back being" background
            #     self._is_background = 0
            #     return self.apply(self, frame)

            self.x, self.y = centroids[label].astype(int)
            mask = labels == label

            if self.max_size > 0:
                mask &= BokehEffect.get_radial_mask((self.x, self.y), self.max_size, frame.shape)

        BokehEffect.blur_by_mask(frame, mask)

class BokehEffectNoMedian(BlurOthersEffect):

    def __init__(self, threshold=10, max_size=0, hue_neighborhood=20, minimal_radius=10):
        super().__init__(hue_neighborhood)
        self.max_size = max_size
        self.minimal_radius = minimal_radius
        self.threshold = threshold
        self.hue_median = hue_median

        self.x, self.y = None, None
        self._is_background = None

        self._behavior_initialized = False

    def apply(self, frame):
        if self.x is None:
            return

        hsv = ColorTools.bgr_to_hsv(frame)

        components = ColorTools.convert_boolean_map_to_uint8_map(
            np.abs(hsv[:, :, 0].astype(int) - self.hue_median) >= self.threshold
        )
        # Remove the too-fine components
        components &= cv2.morphologyEx(components, cv2.MORPH_OPEN, np.ones((2 * self.minimal_radius,) * 2))
        # Make the shape more complete
        components |= cv2.morphologyEx(components, cv2.MORPH_CLOSE, np.ones((2 * self.minimal_radius,) * 2))

        # if not self._behavior_initialized:
        #     self._is_background = components[self.y, self.x] == 0  # 0 is AKA Background
        #     self._behavior_initialized = True

        # if self._is_background:
        if components[self.y, self.x] == 0: # 0 is AKA Background

            mask = ColorTools.get_mask_component_at_pos_by_hue(self.hue_median, self.x, self.y, self.threshold, True)
            mask &= (components == 0)

        else:
            n_labels, labels, stats, centroids = cv2.connectedComponentsWithStats(components, 8, cv2.CV_32S)

            label = labels[self.y, self.x]

            # if label == 0: # This is "back being" background
            #     self._is_background = 0
            #     return self.apply(self, frame)

            self.x, self.y = centroids[label].astype(int)
            mask = labels == label

            if self.max_size > 0:
                mask &= BokehEffect.get_radial_mask((self.x, self.y), self.max_size, frame.shape)

        BokehEffect.blur_by_mask(frame, mask)

    def on_mouse_click(self, frame, x, y, is_left):
        self.x, self.y = x, y
        self._behavior_initialized = False

    @staticmethod
    def get_radial_mask(center_p, radius, shape):
        X, Y = np.arange(0, shape[1]), np.arange(0, shape[0])
        X, Y = np.meshgrid(X, Y)
        return (X - center_p[0]) ** 2 + (Y - center_p[1]) ** 2 <= (radius ** 2)


class HueOnlyEffect(VideoEffect):
    def apply(self, frame):
        hsv = ColorTools.bgr_to_hsv(frame)
        hsv[:, :, 2] = 0xFF
        hsv[:, :, 1] = 0xFF
        frame[:] = ColorTools.hsv_to_bgr(hsv)[:]


class DesaturateOthersEffect(VideoEffect):

    def __init__(self, hue_neighborhood):
        super().__init__(True)
        self.hue_neighborhood = hue_neighborhood
        self.x = None
        self.y = None

    def apply(self, frame):
        if self.x is None:
            return
        hsv = ColorTools.bgr_to_hsv(frame)

        mask = ColorTools.get_mask_component_at_pos_by_hue(frame, self.x, self.y, self.hue_neighborhood)

        hsv[:, :, 1] &= mask

        frame[:] = ColorTools.hsv_to_bgr(hsv)[:]

    def on_mouse_click(self, frame, x, y, is_left):
        self.x, self.y = x, y


class ColorTools:
    @staticmethod
    def bgr_to_hsv(frame):
        return cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    @staticmethod
    def hsv_to_bgr(frame):
        return cv2.cvtColor(frame, cv2.COLOR_HSV2BGR)

    @staticmethod
    def get_all_component_with_hue(hue_mask, hue_val, hue_neighborhood):
        mask = cv2.inRange(hue_mask,
                           np.array([hue_val - hue_neighborhood]),
                           np.array([hue_val + hue_neighborhood]))
        # HUE is cyclic
        if hue_val > 255 - hue_neighborhood:
            mask = mask | ((hue_mask <= hue_neighborhood + (hue_val - 0xFF)).astype("uint8") * 0xFF)
        elif hue_val < hue_neighborhood:
            mask = mask | ((hue_mask >= (0xFF - (hue_neighborhood - hue_val))).astype("uint8") * 0xFF)

        return mask

    @staticmethod
    def get_mask_component_at_pos_by_hue(hsv, x, y, hue_neighborhood, input_is_only_hue=False):

        # Only hue can MAKE .all() this world seem .right()
        # Only hue can MAKE the darkness (== low value) bright (== high value)
        # Only hue and hue alone can thrill 'self' like hue do
        # And fill self.heart with love for (... in ... :) only hue
        if input_is_only_hue:
            pass  # I like this form better because otherwise the song will not fit.
        else:
            hsv = hsv[:, :, 0]

        mask = ColorTools.get_all_component_with_hue(hsv, hsv[y, x], hue_neighborhood)

        connected, _ = snm.label(mask, np.ones((3, 3)))
        return connected[y, x] == connected

    @staticmethod
    def convert_boolean_map_to_uint8_map(bmap):
        return bmap.astype("uint8") * 0xFF


class Displayer:
    def __init__(self, path, show_video=True, width=None):
        self.width = width
        self.show_video = show_video
        self.path = path
        self._cap = cv2.VideoCapture(path)
        # self._cap.
        self.frame = None
        self.effects = []
        self.event_handlers = []

    @staticmethod
    def get_median(path, **kwags):
        class MedianCalculator(VideoEffect):
            def __init__(self):
                super().__init__()
                self.cache = []

            def apply(self, frame):
                self.cache.append(frame)

            def get_median(self):
                return np.median(np.array(self.cache), axis=0)

        d = Displayer(path, False, **kwags)
        med = MedianCalculator()
        d.add_effect(med)
        d.play_video()
        return med.get_median()

    @staticmethod
    def get_hue_median(path, **kwags):
        class MedianCalculator(VideoEffect):
            def __init__(self):
                super().__init__()
                self.cache = []

            def apply(self, frame):
                self.cache.append(ColorTools.bgr_to_hsv(frame)[:, :, 0])

            def get_median(self):
                return np.median(np.array(self.cache), axis=0)

        d = Displayer(path, False, **kwags)
        med = MedianCalculator()
        d.add_effect(med)
        d.play_video()
        return med.get_median()

    @property
    def frame_count(self):
        return self._cap.get(cv2.CAP_PROP_FRAME_COUNT)

    @property
    def next_frame(self):
        return self._cap.get(cv2.CAP_PROP_POS_FRAMES)

    @next_frame.setter
    def next_frame(self, value):
        self._cap.set(cv2.CAP_PROP_POS_FRAMES, value)

    @property
    def video_resolution(self):
        return (
            self._cap.get(cv2.CAP_PROP_FRAME_WIDTH),
            self._cap.get(cv2.CAP_PROP_FRAME_HEIGHT)
        )

    def play_video(self, frame_rate=None):
        if frame_rate is None:
            frame_rate = self._cap.get(cv2.CAP_PROP_FPS)
        if frame_rate > 0:
            frame_rate = max(1, int(1000 / frame_rate))

        if self.show_video:
            cv2.namedWindow("video")
            cv2.setMouseCallback("video", self._on_mouse_event)

        while self._cap.isOpened():
            ret, frame = self._cap.read()

            if not ret:
                break

            if self.width is not None:
                scale = float(self.width) / frame.shape[1]
                frame = cv2.resize(frame, (0, 0), fx=scale, fy=scale, interpolation=cv2.INTER_CUBIC)

            t = time.time() * 1000

            self.frame = frame

            for effect in self.effects:
                effect.apply(frame)

            if self.show_video:
                cv2.imshow('video', frame)

                t = int(time.time() * 1000 - t)

                if cv2.waitKey(max(1, frame_rate - t)) & 0xFF == ord('q'):
                    break

        self._cap.release()
        cv2.destroyAllWindows()

    def add_effect(self, effect: VideoEffect):
        self.effects.append(effect)
        if effect.mouse_click_integration:
            self.event_handlers.append(effect.on_mouse_click)

    def _on_mouse_event(self, event, x, y, flags, param):
        # grab references to the global variables
        # global refPt, cropping

        # if the left mouse button was clicked, record the starting
        # (x, y) coordinates and indicate that cropping is being
        # performed
        if event == cv2.EVENT_LBUTTONDOWN or event == cv2.EVENT_RBUTTONDOWN:
            for eh in self.event_handlers:
                eh(self.frame, x, y, event == cv2.EVENT_LBUTTONDOWN)

        # check to see if the left mouse button was released
        # elif event == cv2.EVENT_LBUTTONUP:
        #     pass
        # # record the ending (x, y) coordinates and indicate that
        # # the cropping operation is finished
        # refPt.append((x, y))
        # cropping = False
        #
        # # draw a rectangle around the region of interest
        # cv2.rectangle(image, refPt[0], refPt[1], (0, 255, 0), 2)
        # cv2.imshow("video", image)

    def set_mouse_click_callback(self):
        pass


if __name__ == "__main__":
    med = ColorTools.bgr_to_hsv(
        Displayer.get_median("woman_on_floor.mp4", width=720).astype(np.uint8))[:, :, 0]

    disp = Displayer("woman_on_floor.mp4", width=720)
    disp.add_effect(BokehEffect(med, 9, minimal_radius=30))
    disp.play_video()
    pass
