import time

import cv2
import cv2.cv2
import numpy as np

from color_tools import ColorTools
from effects.bokeh import BokehEffectNoMedian
from video_effect import VideoEffect


class VideoDisplayer:
    @staticmethod
    def get_median(path, **kwags):
        class MedianCalculator(VideoEffect):
            def __init__(self):
                super().__init__()
                self.cache = []

            def apply(self, frame):
                self.cache.append(frame)

            def get_median(self):
                return np.median(np.array(self.cache), axis=0)

        d = VideoDisplayer(path, False, **kwags)
        med = MedianCalculator()
        d.add_effect(med)
        d.play()
        return med.get_median()

    def __init__(self, path, show_video=True, width=None):
        self.width = width
        self.show_video = show_video
        self.path = path
        self._cap = cv2.VideoCapture(path)
        self.frame = None
        self.effects = []
        self.event_handlers = []

    @property
    def frame_count(self):
        return self._cap.get(cv2.CAP_PROP_FRAME_COUNT)

    @property
    def next_frame(self):
        return self._cap.get(cv2.CAP_PROP_POS_FRAMES)

    @next_frame.setter
    def next_frame(self, value):
        self._cap.set(cv2.CAP_PROP_POS_FRAMES, value)

    @property
    def resolution(self):
        return (
            self._cap.get(cv2.CAP_PROP_FRAME_WIDTH),
            self._cap.get(cv2.CAP_PROP_FRAME_HEIGHT)
        )

    def play(self, frame_rate=None, loop=False):
        if frame_rate is None:
            frame_rate = self._cap.get(cv2.CAP_PROP_FPS)
        if frame_rate > 0:
            frame_rate = max(1, int(1000 / frame_rate))

        if self.show_video:
            cv2.namedWindow("video")
            cv2.setMouseCallback("video", self._on_mouse_event)

        while self._cap.isOpened():
            ret, frame = self._cap.read()

            if not ret:
                if loop:
                    self.next_frame = 0
                    continue
                else:
                    break

            if self.width is not None:
                scale = float(self.width) / frame.shape[1]
                frame = cv2.resize(frame, (0, 0), fx=scale, fy=scale, interpolation=cv2.INTER_CUBIC)

            t = time.time() * 1000

            self.frame = frame

            for effect in self.effects:
                effect.apply(frame)

            if self.show_video:
                cv2.imshow('video', frame)

                t = int(time.time() * 1000 - t)

                if cv2.waitKey(max(1, frame_rate - t)) & 0xFF == ord('q'):
                    break

        self._cap.release()
        cv2.destroyAllWindows()

    def add_effect(self, effect: VideoEffect):
        self.effects.append(effect)
        if effect.mouse_click_integration:
            self.event_handlers.append(effect.on_mouse_click)

    def _on_mouse_event(self, event, x, y, flags, param):
        if event == cv2.EVENT_LBUTTONDOWN or event == cv2.EVENT_RBUTTONDOWN:
            for eh in self.event_handlers:
                eh(self.frame, x, y, event == cv2.EVENT_LBUTTONDOWN)

    def set_mouse_click_callback(self):
        pass


if __name__ == "__main__":
    med = ColorTools.bgr_to_hsv(
        VideoDisplayer.get_median("woman_on_floor.mp4", width=720).astype(np.uint8))[:, :, 0]

    disp = VideoDisplayer("woman_on_floor.mp4", width=720)
    disp.add_effect(BokehEffectNoMedian(med, hue_neighborhood=9, minimal_radius=5, max_size=50))
    disp.play(loop=True)
